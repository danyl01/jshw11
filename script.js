document.querySelector(".password-form").addEventListener("click", (event) => {
    if (event.target.classList.contains("fa-eye")) {
        event.target.style.display = "none";
        event.target.nextElementSibling.style.display = "block";
        event.target.previousElementSibling.setAttribute("type","text");
    } else if (event.target.classList.contains("fa-eye-slash")) {
        event.target.style.display = "none";
        event.target.previousElementSibling.style.display = "block";
        event.target.previousElementSibling.previousElementSibling.setAttribute("type", "password");
    } else if (event.target.tagName === "BUTTON") {
        event.preventDefault();
        let password1 = document.querySelector("#password").value;
        let password2 = document.querySelector("#repeat-password").value;
        if (password1 === password2) {
            alert("You are welcome");
        } else {
            alert("Потрібно ввести однакові значення");
        }
    }
})